
(() => {
  const cache = {
  };

  const module = {
    exports: {}
  };

  function fetchFile(fullPath) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', fullPath, false);
    xhr.send();

    if (xhr instanceof Error) {
      throw TypeError();
    }
    return xhr.response;
  }
  
  function require(path) {
    if (cache[path]) {
      return cache[path];
    }

    const file = fetchFile(path);
    const anonFunc = new Function('module', 'exports', file);
    anonFunc(module, module.exports);

    cache[path] = module.exports;

    return (module, module.exports);
  }

  require.cache = cache;
  window.require = require; 
})();
