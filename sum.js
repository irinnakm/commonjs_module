let count = 0;

function sum(a, b) {
  count++;
  return a + b;
}

module.exports.sum = sum;
module.exports.count = count;
